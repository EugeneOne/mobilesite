﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class UserModel
    {                   
        public string UserName { get; set; }  
        public string Password { get; set; }
        public string AuthToken { get; set; }
        public string AuthError { get; set; }

    }
}
