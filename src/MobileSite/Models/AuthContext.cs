﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class AuthContext : DbContext
    {
        public DbSet<Auth> Auth { get; set; }
        public AuthContext(DbContextOptions<AuthContext> options)
            : base(options)
        {
        }
    }
}
