﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class DmsObjectRestResponse
    {
        public string Id { get; set; }
        public string TypeUid { get; set; }
        public string Name { get; set; }
        public DmsObjectRestResponse Folder { get; set; }
        public DmsObjectRestResponse Document { get; set; }
    }
}
