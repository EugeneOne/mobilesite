﻿using System.Collections.Generic;

namespace MobileSite.Models
{
    public class SimpleTaskInfoModel
    {

        public string Id { get; set; }
        public string TypeUid { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string ProjectName { get; set; }
        public string WorkflowBookmarkProcessName { get; set; }
        public List<string> DocumentAttachmentsNames { get; set; }
        public string EndDate { get; set; }
        public List<string> CommentsStrings { get; set; }
    }

    public class SimpleDocumentApprovementTask : SimpleTaskInfoModel
    {
        public List<DmsObjectRestResponse> Items { get; set; } // для первой загрузки
        public Dictionary<string, string> DocumentVersions { get; set; }//для полного отображения

    }
}
