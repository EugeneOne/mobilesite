﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class DmsObjectModel
    {
        public string Id { get; set; }
        public string TypeUid { get; set; }
        public string Name { get; set; }
        public string FolderId { get; set; }
    }

    public class DmsObjectViewModel
    {
        public string Id { get; set; }
        public string FolderId { get; set; }
        public List<DmsObjectModel> DmsObjects { get; set; }
    }

}
