﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class Auth
    {
        public int Id { get; set; }
        public string AuthToken { get; set; }
        public string CurrentUserId { get; set; }
        public string SessionToken { get; set; }
    }
}
