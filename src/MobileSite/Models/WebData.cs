﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Models
{
    public class WebData
    {


        public  WebDataItem[] Items { get; set; }

     

       
        public virtual string Value { get; set; }
    }


    public class WebDataItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public  WebData Data { get; set; }
        public  WebData[] DataArray { get; set; }
    }
}
