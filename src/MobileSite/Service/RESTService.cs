﻿using MobileSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using System.IO;          
using Newtonsoft.Json;

namespace MobileSite.Services
{
    public class RESTService
    {                        
        private static string ElmaServer = @"https://elma.elewise.com";
        public static string AuthToken { get; set; }
        public static string SessionToken { get; set; }
        private static string appToken = "285C8352AA7C67BFF882E4F236DECF51098C141AFB33A2AA4F7B34B4B3CEEF5DA30C848591DA55D5226C5D8D2C36432B12A5EF86C3D2EDF7E7C5781EC9D4E14A";
        
        private static JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            DateParseHandling = DateParseHandling.DateTime,
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
        };

        /// <summary>
        /// Выполнить запрос на основании данных
        /// </summary>
        /// <param name="restData">Данные</param>
        /// <returns>Ответ сервера</returns>
        private static HttpWebResponse DoRestExecute(RestData restData, Dictionary<string, string> headers = null)
        {
            //Создаем экземпляр запроса по URL
            var HttpWReq = (HttpWebRequest)WebRequest.Create(restData.Url);

            //Указываем метод запроса
            HttpWReq.Method = restData.HTTPMethod;
            HttpWReq.Credentials = CredentialCache.DefaultCredentials;
            //Добавляем токен приложения
            HttpWReq.Headers["ApplicationToken"] = appToken;
            //Добавляем токен авторизации, если есть
            if (!string.IsNullOrWhiteSpace(AuthToken))
            {
                HttpWReq.Headers["AuthToken"] = AuthToken;
            }
            //Добавляем токен сессии, если есть
            if (!string.IsNullOrEmpty(SessionToken))
            {
                HttpWReq.Headers["SessionToken"] = SessionToken;
            }
            HttpWReq.Accept = "application/json; charset=utf-8";
            HttpWReq.Headers["WebData-Version"] = "2.0";
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    HttpWReq.Headers[header.Key] = header.Value;
                }
            }
            var encoding = new UTF8Encoding();

            //Если метод POST, тогда добавляем тип контента и вставляем данные
            if (restData.HTTPMethod == "POST")
            {
                var byteData = encoding.GetBytes(restData.Data);
                HttpWReq.ContentType = "application/json; charset=utf-8";

                //HttpWReq.ContentLength = byteData.Length;
                using (Stream requestStream = HttpWReq.GetRequestStreamAsync().GetAwaiter().GetResult())
                {
                    requestStream.Write(byteData, 0, byteData.Length);
                }
            }
            return (HttpWebResponse)HttpWReq.GetResponseAsync().GetAwaiter().GetResult();//.GetResponse();
        }

        /// <summary>
        /// Выполнить запрос
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="url">URL</param>
        /// <param name="requestData">Сериализованные данные</param>
        /// <param name="httpMethod">Метод запроса (post, get)</param>
        /// <returns>Десериализованный ответ с сервера</returns>
        public static T RestExecute<T>(string url, object requestData, string httpMethod, Dictionary<string, string> addHeaders = null)
        {
            var serializedObject = JsonConvert.SerializeObject(requestData);
            var g = new RestData(ElmaServer + url, serializedObject, httpMethod);
            HttpWebResponse httpWResp = null;
            try
            {
                httpWResp = DoRestExecute(g, addHeaders);
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Response);
            }

            var result = "";
            if (httpWResp != null && httpWResp.StatusCode == HttpStatusCode.OK)
            {
                using (var sr = new StreamReader(httpWResp.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                }
            }
            else
            {
                
            }
            return JsonConvert.DeserializeObject<T>(result, JsonSettings);
        }
    }
}
