﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MobileSite.Managers;
using MobileSite.Models;

namespace MobileSite.Controllers
{
    public class TaskController : Controller
    {
        public ActionResult TaskList()
        {
            var model = new DataManager().GetSimpleTasks();
            if (model == null) model = new List<SimpleTaskInfoModel>();
            return View(model);
        }

        public ActionResult TaskBasePage(string Id)
        {
            
            return View();
        }
   } 
}
