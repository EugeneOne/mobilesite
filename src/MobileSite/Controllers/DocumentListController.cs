﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobileSite.Models;
using MobileSite.Services;

namespace MobileSite.Controllers
{
    public class DocumentListController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id, string parentId)
        {
            var model = new DmsObjectViewModel();
            model.Id = id;
            model.FolderId = parentId;
            if (String.IsNullOrWhiteSpace(model.Id))
            {
                model.Id = "1"; // по умолчанию общие папки
                model.FolderId = parentId;
             
            }
          
            model.DmsObjects = new List<DmsObjectModel>();
            model = GetViewModel(model);

            return View(model);
        }
                                                                    
                    
        public DmsObjectViewModel GetViewModel(DmsObjectViewModel model)
        {
            if (ModelState.IsValid)
            {



                var urlDms =
                    string.Format(
                        "{0}/API/REST/Entity/Query?type={1}&q={2}&limit=50&offset=0&sort=CreationDate-desc&filterProviderUid={3}&filterProviderData=&filter=",
                        "", "1e486899-d718-41d8-9a11-ce18b3262fb7", "Folder=" + model.Id,
                        "77a266e2-e8df-41ab-82ee-8fd93db77eec");
                var docs = RESTService.RestExecute<List<DmsObjectRestResponse>>(urlDms, null, "GET");
                if (docs == null)
                {
                    return model;
                }
                else
                {
                    foreach (var item in docs)
                    {
                        //if (item.DataArray != null)
                        //{
                        //    var newdms = new DmsObjectModel();
                        //    foreach (var att in item.DataArray)
                        //    {
                        //        if (item.Name == "Id")
                        //        {
                        //            newdms.Id = item.Value;
                        //        }
                        //        else if (item.Name == "TypeUid")
                        //        {
                        //            newdms.TypeUid = item.Value;
                        //        }
                        //        else if (item.Name == "Name")
                        //        {
                        //            newdms.Name = item.Value;
                        //        }
                        //        else if (item.Name == "Folder")
                        //        {
                        //            //newdms.FolderId = item.Value;
                        //            newdms.FolderId = model.Id;
                        //        }
                        //    }

                        var newdms = new DmsObjectModel();
                        newdms.FolderId = item.Folder != null ? item.Folder.Id : "";
                        newdms.Id = item.Id;
                        newdms.Name = item.Name;
                        newdms.TypeUid = item.TypeUid;
                        model.DmsObjects.Add(newdms);
                    }
                }
            }
            return model;
        }
    }
}
