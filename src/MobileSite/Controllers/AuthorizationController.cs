﻿using System;
using System.Linq;
using MobileSite.Models;
using Microsoft.AspNetCore.Mvc;
using MobileSite.Services;
using RestSharp;

namespace MobileSite.Controllers
{
    public class AuthorizationController : Controller
    {
        private AuthContext db;
        public AuthorizationController(AuthContext context)
        {
            db = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var cookieVal = Request.Cookies["session"];
            if (!String.IsNullOrEmpty(cookieVal) && db.Auth.Where(p => p.SessionToken == cookieVal).Count() > 0)
                return RedirectToAction("Tasks", "Home");

            var t = db.Auth.ToList();
            var model = new UserModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(UserModel model)
        {
            if (ModelState.IsValid)
            {
                var urlAuth = string.Format("/API/REST/Authorization/LoginWith?username={0}", model.UserName);
                var auth = RESTService.RestExecute<Auth>(urlAuth, model.Password, "POST");
                if (auth == null)
                {
                    model.AuthError = "Ошибка ввода логина либо пароля";
                    return View(model);
                }
                if (auth.CurrentUserId != null && !string.IsNullOrWhiteSpace(auth.SessionToken))
                {
                    var elements = db.Auth.Where(p => p.CurrentUserId == auth.CurrentUserId);
                    if (elements.Count() > 0)
                    {
                        var auth_old = elements.First();
                        auth_old.SessionToken = auth.SessionToken;
                        auth_old.AuthToken = auth.AuthToken;
                        db.Auth.Update(auth_old);
                    }
                    else
                        db.Auth.Add(auth);
                    db.SaveChanges();
                    Response.Cookies.Delete("session");
                    Response.Cookies.Append("session", auth.SessionToken);

                    return RedirectToAction("Tasks", "Home");
                }
            }
            return View(model);
        }
    }
}
