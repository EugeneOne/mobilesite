﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobileSite.Models;
using MobileSite.Services;

namespace MobileSite.Controllers
{



    public class DocumentApprovementController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id)
        {
            var model = new SimpleDocumentApprovementTask();
            model.Id = id;
            model.Items = new List<DmsObjectRestResponse>();
            model.DocumentAttachmentsNames = new List<string>();
            model.CommentsStrings = new List<string>();
            model.DocumentVersions = new Dictionary<string, string>();
            model = getModel(model);
            return View(model);
        }



        [HttpPost]
        public SimpleDocumentApprovementTask getModel(SimpleDocumentApprovementTask model)
        {
            if (ModelState.IsValid && !String.IsNullOrWhiteSpace(model.Id))
            {


                var urlDms = string.Format("{0}/API/REST/Entity/Load?type={1}&id={2}",
                "", "ddedc971-594a-4028-9781-f218b4658b92", model.Id);
                var doctask = RESTService.RestExecute<SimpleDocumentApprovementTask>(urlDms, "", "GET");
                if (doctask == null)
                {
                    return model;
                }
                else
                {
                    foreach (var tag in doctask.Items)
                    {


                        //if (tag.Name == "Id")
                        //{
                        //    model.Id = tag.Value;
                        //}
                        //else
                        //if (tag.Name == "TypeUid")
                        //{
                        //    model.TypeUid = tag.Value;
                        //}
                        //else
                        //    if (tag.Name == "Subject")
                        //{
                        //    model.Subject = tag.Value;
                        //}
                        //else
                        //        if (tag.Name == "Description")
                        //{
                        //    model.Subject = tag.Value;
                        //}
                        //else
                        //        if (tag.Name == "EndDate")
                        //{
                        //    model.EndDate = tag.Value;
                        //}
                        //else
                        //if (tag.Name == "Comments")
                        //{
                        //    foreach (var commenttag in tag.DataArray)
                        //    {
                        //        foreach (var comment in commenttag.Items)
                        //        {
                        //            if (comment.Name == "Name")
                        //            {
                        //                model.CommentsStrings.Add(comment.Value);
                        //            }
                        //        }
                        //    }

                        //}
                        //else
                        //    if (tag.Name == "DocumentAttachments" && tag.DataArray != null)
                        //{
                        //    foreach (var documenttag in tag.DataArray)
                        //    {
                        //        foreach (var document in documenttag.Items)
                        //        {
                        //            if (document.Name == "Name")
                        //            {
                        //                model.DocumentAttachmentsNames.Add(document.Value);
                        //            }
                        //        }
                        //    }

                        //}
                        //else
                        //        if (tag.Name == "Project" && tag.Data != null && tag.Data.Items != null)
                        //{
                        //    foreach (var projecttag in tag.Data.Items)
                        //    {
                        //        if (projecttag.Name == "Name")
                        //        {
                        //            model.ProjectName = projecttag.Value;
                        //        }
                        //    }
                        //}
                        //else
                        //            if (tag.Name == "WorkflowBookmark" && tag.Data != null && tag.Data.Items != null)
                        //{
                        //    foreach (var workflowtag in tag.Data.Items)
                        //    {
                        //        if (workflowtag.Name == "ProcessName")
                        //        {
                        //            model.WorkflowBookmarkProcessName = workflowtag.Value;
                        //        }
                        //    }
                        //}
                        //else
                        //            if (tag.Name == "Items" && tag.Data != null && tag.Data.Items != null)
                        //{
                        //    foreach (var itemstag in tag.Data.Items)
                        //    {
                        //        if (itemstag.Name == "Id")
                        //        {
                        //            model.Items.Add(workflowtag.Value);
                        //        }
                        //    }
                        //}


                    }

                    //А теперь получить документы
                    foreach (var element in model.Items)
                    {
                        var urlDocs = string.Format("{0}/API/REST/Entity/Load?type={1}&id={2}",
                           "", "ef33e1d3-b229-4d3c-90ca-0b0be5a19459", element);
                        var docs = RESTService.RestExecute<DmsObjectRestResponse>(urlDocs, "", "GET");
                        if (docs != null)
                        {
                            
                                model.DocumentVersions.Add(docs.Document.Id, docs.Document.Name);
                            

                    }
                        //if (docs != null && docs.Document != null && docs.Document.Items != null)
                        //{
                        //    string docid = "";
                        //    string docname = "";
                        //    foreach (var tag in docs.Document.Items)
                        //    {
                        //        if (tag.Name == "Name")
                        //        {
                        //            docname = tag.Value;
                        //        }
                        //        else if (tag.Name == "Id")
                        //        {
                        //            docid = tag.Value;
                        //        }

                        //    }
                        //    model.DocumentVersions.Add(docid, docname);
                        //}

                    }



                }
            }
            return model;
        }
    }
}
