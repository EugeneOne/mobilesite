﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobileSite.Services;
using MobileSite.Managers;

namespace MobileSite.Controllers
{
    public class HomeController : Controller
    {
        private DataManager manager
        {
            get {
                return new DataManager();
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Tasks()
        {
            var taskList = manager.GetSimpleTasks();          
            return View(taskList);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
