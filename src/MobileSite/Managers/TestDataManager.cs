﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MobileSite.Models;

namespace MobileSite.Managers
{
    public class TestDataManager
    {
        public IEnumerable<SimpleTaskInfoModel> GetTestTasks()
        {
            var result = new List<SimpleTaskInfoModel>();

            var task = new SimpleTaskInfoModel();
            task.Description = "testDescr1";
            task.CommentsStrings = new List<string> {"strings"};
            task.EndDate = DateTime.Now.ToString();
            task.Subject = "subject";
            result.Add(task);

            var task2 = new SimpleTaskInfoModel();
            task2.Description = "testDescr2";
            task2.CommentsStrings = new List<string> { "strings2" };
            task2.EndDate = DateTime.Now.ToString();
            task2.Subject = "subject2";
            result.Add(task2);

            var task3 = new SimpleTaskInfoModel();
            task3.Description = "testDescr3";
            task3.CommentsStrings = new List<string> { "strings3" };
            task3.EndDate = DateTime.Now.ToString();
            task3.Subject = "subject3";
            result.Add(task3);

            return result;
        }
    }
}
