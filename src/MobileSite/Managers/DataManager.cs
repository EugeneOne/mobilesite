﻿using MobileSite.Models;
using MobileSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileSite.Managers
{
    public class DataManager
    {
        public List<SimpleTaskInfoModel> GetSimpleTasks()
        {
            var taskBaseTypeUid = "f532ef81-20e1-467d-89a4-940c57a609bc";
            var eqlQuery = "Executor = CurrentUser()";
            var filter = "CompleteStatus:Active";
            var sort = "CreationDate-desc";
            var filterProviderUid = "f532ef81-20e1-467d-89a4-940c57a609bc";
            var urlTasks = string.Format("/API/REST/Entity/Query?type={0}&q={1}&limit={2}&offset={3}&sort={4}&filterProviderUid={5}&filterProviderData={6}&filter={7}", taskBaseTypeUid, eqlQuery, 100, 0, sort, filterProviderUid, null, filter);
            var taskList = RESTService.RestExecute<List<SimpleTaskInfoModel>>(urlTasks, "", "GET");

            return taskList;
        }

    }
}
